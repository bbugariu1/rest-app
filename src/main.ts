import { NestFactory } from "@nestjs/core";
import { AppModule } from "./modules/app/app.module";
import { VersioningType } from "@nestjs/common";
import { NestExpressApplication } from "@nestjs/platform-express";

const PORT: string = process.env.PORT || "3000";

async function bootstrap()
{
    const app: NestExpressApplication = await NestFactory.create<NestExpressApplication>(AppModule, { logger: ["debug"] });

    app.enableVersioning({ type: VersioningType.URI, prefix: "v" });
    app.enableCors();

    await app.listen(PORT,
      () =>
      {
          // eslint-disable-next-line no-console
          console.log(`Server listening on PORT: ${PORT}.`);
      });

}
bootstrap();
