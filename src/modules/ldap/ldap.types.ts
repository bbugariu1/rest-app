export interface ILdapLogin
{
    username: string;
    password: string;
}

export enum LdapRoutes
{
    Ldap = "ldap"
}

export interface ILdapUser
{
    dn: string;
    cn: string;
    firstName: string;
    lastName: string;
    email: string;
    resetPasswordToken: string;
    objectClass: string[];
    servicesAccess: string[];
    activeAccountStatus: string;
    jobtitle: string;
    department: string;
    location: string;
    reportTo: string;
    lineManager: string;
    description: string;

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [key: string]: any;
}
