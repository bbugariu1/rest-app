import { Module } from "@nestjs/common";
import { LdapService } from "../ldap/ldap.service";
import LdapController from "./ldap.controller";

@Module({
    imports: [],
    controllers: [LdapController],
    providers: [LdapService]
})
export class LdapModule { }
