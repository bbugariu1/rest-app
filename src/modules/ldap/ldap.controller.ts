import { Controller, Get } from "@nestjs/common";
import { ILdapUser, LdapRoutes } from "./ldap.types";

@Controller({
    version: "1",
    path: LdapRoutes.Ldap
})
/**
 * TODO: Guard all routes under a auth middleware
 */
export default class LdapController
{
    @Get()
    public async getUsers(): Promise<ILdapUser[]>
    {
        return [];
    }
}
