import { Injectable } from "@nestjs/common";
import Ldap from "ldap-async";
import { ILdapLogin, ILdapUser } from "./ldap.types";

@Injectable()
export class LdapService
{
    public connect(params: ILdapLogin): Ldap
    {
        return new Ldap({
            url: process.env.LDAP_URL,
            poolSize: 5,
            bindDN: `cn=${params.username},${process.env.LDAP_SEARCH}`,
            bindCredentials: params.password,
            timeout: 30000
        });
    }

    public async getUser(params: ILdapLogin): Promise<ILdapUser>
    {
        return this.connect(params).get(`cn=${params.username},${process.env.LDAP_SEARCH}`);
    }
}
