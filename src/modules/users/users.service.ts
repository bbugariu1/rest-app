import { Injectable } from "@nestjs/common";
import { User } from "@prisma/client";
import { PrismaService } from "../prisma/prisma.service";
import { Prisma } from "@prisma/client";
import { IUpdateToken } from "./types";

@Injectable()
export class UsersService
{
    constructor(
        private prismaService: PrismaService
    ) { }

    public async findAll(): Promise<User[]>
    {
        return this.prismaService.user.findMany();
    }

    public async findUnique(args: Prisma.UserFindUniqueArgs): Promise<User>
    {
        return this.prismaService.user.findUnique(args);
    }

    public async create(data: Prisma.UserCreateInput): Promise<User>
    {
        return this.prismaService.user.create({ data });
    }

    public async update(args: Prisma.UserUpdateArgs): Promise<User>
    {
        return this.prismaService.user.update(args);
    }

    public async updateToken(data: IUpdateToken): Promise<User>
    {
        const { username, token } = data;

        return this.prismaService.user.update({
            where: { username },
            data: { token }
        });
    }
}
