export interface IUpdateToken
{
    username: string;
    token: string;
}
