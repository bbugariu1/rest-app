import { Module } from "@nestjs/common";
import { PassportModule } from "@nestjs/passport";
import { LdapService } from "../ldap/ldap.service";
import { AuthController } from "./auth.controller";
import { AuthService } from "./auth.service";
import { LocalStrategy } from "./local.strategy";
import { JwtModule } from "@nestjs/jwt";
import { PrismaModule } from "../prisma/prisma.module";
import { UsersService } from "../users/users.service";

@Module({
    imports: [
        PassportModule,
        JwtModule.register({
            secret: process.env.JWT_SECRET,
            signOptions: { expiresIn: "60s" }
        }),
        PrismaModule
    ],
    controllers: [AuthController],
    providers: [AuthService, LocalStrategy, LdapService, UsersService]
})
export class AuthModule { }
