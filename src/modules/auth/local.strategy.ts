import { Strategy } from "passport-local";
import { PassportStrategy } from "@nestjs/passport";
import { Injectable, UnauthorizedException } from "@nestjs/common";
import { AuthService } from "./auth.service";
import { IAuthParams, ILoginDetails } from "./auth.types";

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy)
{
    constructor(private authService: AuthService)
    {
        super();
    }

    public async validate(username: string, password: string): Promise<ILoginDetails>
    {
        const userAuthDetails: IAuthParams = { username, password };
        const userLoginDetails: ILoginDetails = await this.authService.validateUser(userAuthDetails);

        if (!userLoginDetails)
        {
            throw new UnauthorizedException();
        }

        return {
            ...userLoginDetails
        };
    }
}
