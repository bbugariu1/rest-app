import { Injectable } from "@nestjs/common";
import { LdapService } from "../ldap/ldap.service";
import { JwtService } from "@nestjs/jwt";
import { User } from "@prisma/client";
import { IAuthParams, ILoginDetails, ILoginResponse } from "./auth.types";
import { UsersService } from "../users/users.service";
import { ILdapUser } from "../ldap/ldap.types";

@Injectable()
export class AuthService
{
    constructor(
        private ldapService: LdapService,
        private jwtService: JwtService,
        private userService: UsersService,
    ) { }

    public async validateUser(userAuthDetails: IAuthParams): Promise<ILoginDetails | null>
    {
        try
        {
            const { username, password } = userAuthDetails;
            const ldapUser: ILdapUser = await this.ldapService.getUser({ username, password });
            let currentUser: User = await this.userService.findUnique({ where: { username } });

            currentUser = await this.createUserIfNotExists(currentUser, username);

            const userLoginDetails: ILoginDetails = {
                id: currentUser.id,
                ...ldapUser
            };

            return userLoginDetails;
        }
        catch (error)
        {
            return null;
        }
    }

    private async createUserIfNotExists(currentUser: User | null, username: string): Promise<User>
    {
        if (!currentUser)
        {
            return this.userService.create({ username });
        }

        return currentUser;
    }

    /**
     * @TODO Handle catch block for prisma errors
     */
    public async login(user: ILoginDetails): Promise<ILoginResponse>
    {
        const { id, cn, ...rest } = user;
        const loginPayload: Partial<ILoginDetails> = { cn, id };
        const token: string = this.jwtService.sign(loginPayload);

        await this.userService.updateToken({ username: cn, token });

        return { token, cn, ...rest };
    }
}
