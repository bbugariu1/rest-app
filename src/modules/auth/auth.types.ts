import { ILdapUser } from "../ldap/ldap.types";

export enum AuthRoutes
{
    Auth = "auth",
    Login = "login"
}

export interface IAuthParams
{
    username: string;
    password: string;
}

export interface ILoginResponse extends ILdapUser
{
    token: string;
}

export interface ILoginDetails extends ILdapUser
{
    id: number;
}
