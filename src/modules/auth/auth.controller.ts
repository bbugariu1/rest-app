import { Controller, Get, Post, Request, UseGuards } from "@nestjs/common";
import { AuthService } from "./auth.service";
import { LocalAuthGuard } from "./local-auth.guard";
import { AuthRoutes, ILoginDetails, ILoginResponse } from "./auth.types";

@Controller({
    version: "1",
    path: AuthRoutes.Auth
})
export class AuthController
{
    constructor(private authService: AuthService) { }

    @UseGuards(LocalAuthGuard)
    @Post(AuthRoutes.Login)
    public async login(@Request() req: ILoginDetails): Promise<ILoginResponse>
    {
        return this.authService.login(req.user);
    }

    @Get("/test")
    public test (): string {
        console.log(123)
        return "test"
    }
}
